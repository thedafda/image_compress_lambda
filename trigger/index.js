'use strict';
var Bluebird = require('bluebird');
var AWS = require('aws-sdk');
AWS.config.setPromisesDependency(Bluebird);

AWS.config.update({region:'us-east-1'});

var s3 = new AWS.S3({ apiVersion: '2006-03-01' });
var sns = new AWS.SNS();

var OPTIMIZE_TOPIC_ARN = "arn:aws:sns:us-east-1:768368567485:image-test"

function getEvent(msg){
    var event = {
        Message: JSON.stringify(msg), 
        Subject: "OPTIMIZE_IMAGE",
        TopicArn: OPTIMIZE_TOPIC_ARN
    };
    return event;
}

function isValidImage(key){
    return (/\.png$/.test(key) || /\.jpg$/.test(key))
}

function getAllImageInBucket(params){
    return s3
        .listObjects(params)
        .promise()
        .then((o) => {
            return o.Contents;
        })
        .then((files) => {
            return files.filter((f) => {
                return isValidImage(f.Key);
            })
        })
}

exports.handler = function(event, context, callback) {

    //TODO: integrate with API gateway parameters

    let params = {
        Bucket: "image.compress.test.bucket",
        Delimiter: '',
        Prefix: ''
    };

    getAllImageInBucket(params)
        .then((imgFiles) => {
            return imgFiles.map((o) => {
                return {
                    Bucket: params.Bucket,
                    Key: o.Key
                };
            });
        })
        .then((bkList) => {
            console.log("Image Keys:", bkList);
            return bkList;
        })
        .then((bkList) => {
            var pArr = bkList.map((o) => {
                var event = getEvent(o);
                console.log("Publishing Event", event)
                return sns.publish(event).promise().then((d) => {
                    console.log("Published Event", event)
                    return d;
                });
            });
            return Bluebird.all(pArr);
        })
        .then((data) => {
            console.log("Done", data);
        })
        .catch((err) => {
            console.error(err);
        });

};



exports.handler()

